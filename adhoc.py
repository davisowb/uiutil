
import time
from uiutil.frame.frame import BaseFrame
from uiutil.window.root import RootWindow


class AdhocFrame(BaseFrame):

    def __init__(self,
                 *args,
                 **kwargs):
        super(AdhocFrame, self).__init__(*args, **kwargs)

        self.button(text=u'Click Me to block for 10s!',
                    width=30,
                    command=self.click_timer,
                    sticky=u'EW')

        self.row.next()

        self.button(text=u'Click Me!',
                    width=30,
                    command=self.click,
                    sticky=u'EW')

    @staticmethod
    def click_timer():
        time.sleep(10)
        print(u'click timer done')

    @staticmethod
    def click():
        print(u'click done')


class AdhocWindow(RootWindow):

    def __init__(self,
                 *args,
                 **kwargs):
        super(AdhocWindow, self).__init__(*args, **kwargs)

    def _setup(self):
        self.title(u'AdhocWindow')
        self.adhoc_frame = AdhocFrame(parent=self._main_frame)


w = AdhocWindow(width=350, height=250)
