
uiutil
======

Uiutil sits on top of ttk/tkinter for cleaner UI code.

Visit http://uiutil.readthedocs.io/en/latest/ for more information.
